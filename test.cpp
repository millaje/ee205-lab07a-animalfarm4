///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   23 Mar 2021 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"


using namespace std;
using namespace animalfarm;

int main(){
   //cout << "I am Jerie LOL" <<endl;

   for(int i=0; i<10; i++){
      //cout << Animal::getRandomGender() << endl;
      //cout << Animal::getRandomColor() << endl;
      //cout << Animal::getRandomBool() << endl;
      //cout << Animal::getRandomWeight(2, 3.4) << endl;
      //cout << Animal::getRandomName() << endl;
      //Animal* a = AnimalFactory::getRandomAnimal();
      //cout << a->speak() << endl;
   }
   
   //Node* node
   Node node;
   SingleLinkedList list;
   cout<<"eeeeeempty?"<< boolalpha << list.empty() << endl;

   list.push_front(new Node());
   cout <<"new Node added :)"<< endl;
   cout <<"eeeemppty??"<<boolalpha <<list.empty()<<endl;
   list.push_front(new Node());
   cout <<"anotha new Node added :)"<< endl;
   cout<<"size: " << list.size()<<endl;

   list.pop_front();
   cout<<"node removed"<<endl;
   cout<<"size: "<<list.size()<<endl;

   cout<<"uno node: "<< list.get_first()<<endl;
   list.push_front(new Node());
   list.push_front(new Node());
   list.push_front(new Node());
   cout<<"anotha new Node added :)... new size: " <<list.size()<<endl;

}
















/*#define MAX_LENGTH (9)
std::string randomName() {
   srand (time (NULL) );
   int length = rand() % 6 + 4;
   
   char name[ MAX_LENGTH ]; 
   name[0] = 'A' + ( rand() % 26 );  
   
   for ( int i = 1; i < length ; i++ ) {
      name[i] = 'a' + ( rand() % 26 );
   }

   return name;
}


int main() {
	cout << "TEST: ANIMAL FACTORY" << endl;

	Animal animalTest = AnimalFactory::getRandomAnimal();
   cout << "Random Name = " << animalTest.name << endl;
	cout << "Random Color = " << animalTest.colorName( animalTest.hairColor ) << endl;
	cout << "Random Gender = " << animalTest.genderName( animalTest.gender ) << endl;
	

	return 0;
}
*/

/*
#include <iostream>
#include <random>
#include "animal.hpp"

int main (){
  std::random_device rd;
Animal animalTest = AnimalFactory::getRandomAnimal();
  std::cout << rd() << std::endl;
cout << "Random Color = " << animalTest.colorName( animalTest.hairColor ) << endl;
  return 0;
}



*/



