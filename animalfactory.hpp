
#pragma once
#include <iostream>
#include <string>
#include "animal.hpp"

namespace animalfarm {

class AnimalFactory {
   public:
      static Animal* getRandomAnimal();
};

}
