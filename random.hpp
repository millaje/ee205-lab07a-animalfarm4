#include<iostream>

int maint{
 static const Gender getRandomGender();
 static const Color  getRandomColor();
 static const bool   getRandomBool();
 static const float  getRandomWeight(const float from, const float to);
 static const string getRandomName();

}

