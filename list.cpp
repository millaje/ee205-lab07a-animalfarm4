///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 Mar 2021 
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
//#include <cstdlib>
#include "list.hpp"

using namespace std;

namespace animalfarm{
   const bool SingleLinkedList::empty() const{
      return head == nullptr;
   }
   
   void SingleLinkedList::push_front(Node* newNode){
      if(newNode == nullptr)
         return;
      newNode->next = head;
      head = newNode;
      numOfNodes++;
   }

   Node* SingleLinkedList::pop_front(){
      if(head==nullptr)
         return nullptr;
      Node* returning = head;
      head = head->next;
      numOfNodes--;

      return returning;
   }
   Node* SingleLinkedList::get_first() const{
      return head;
   }

    Node* SingleLinkedList::get_next(const Node* currentNode) const{
       return currentNode->next;
    }

    unsigned int SingleLinkedList::size() const{
       return numOfNodes;
    }
}//end of namespace

