///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {
	

Animal::Animal(){
   cout << ".";
}

Animal::~Animal(){
   cout << "x";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

const enum Gender Animal::getRandomGender(){

   int ranNum = rand() % 2;

   switch (ranNum) {
      case 0: return MALE;
      case 1: return FEMALE;
   }
   return MALE;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

const enum Color Animal::getRandomColor(){
   int ranNum = rand() % 6;
   switch (ranNum) {
      case 0:     return BLACK;
      case 1:     return WHITE;
      case 2:     return RED;
      case 3:     return SILVER;
      case 4:     return YELLOW;
      case 5:     return BROWN;
   }
   return BLACK;
};


string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch (color) {
      case BLACK:    return string("Black"); break;
      case WHITE:    return string("White"); break;
      case RED:      return string("Red"); break;
      case SILVER:   return string("Silver"); break;
      case YELLOW:   return string("Yellow"); break;
      case BROWN:    return string("Brown"); break;
   }
   return string("Unknown");
};

const bool Animal::getRandomBool(){
   int ranNum = rand() % 2;
   switch (ranNum) {
      case 0: return false;
      case 1: return true;
   }
   return true;
}
const float Animal::getRandomWeight(const float from, const float to){
   int ranNum = (rand() % int(to - from));
   float w = from + ranNum;
   return w;
}

const string Animal::getRandomName(){
   int ranNum = rand() %6;
   int l = ranNum + 4;

   int upperCase = rand() % 26 + 65;
   
   string ranName;

   ranName += char (upperCase);
   for(int i=1; i < l; i++){
      int lowerCase = rand() &26 + 97;

      ranName += char(lowerCase);
   }
   return ranName;
}


Animal* AnimalFactory::getRandomAnimal () {
   Animal* newAnimal= NULL;
   int i = rand() % 6;

   switch (i){
      case 0: newAnimal = new Cat(Animal::getRandomName(),Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 1: newAnimal = new Dog( Animal::getRandomName(),   Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 2: newAnimal = new Nunu( Animal::getRandomBool(),   Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 3: newAnimal = new Aku(Animal::getRandomWeight(5, 21), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 4: newAnimal = new Palila(Animal::getRandomName(),   Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 5: newAnimal = new Nene( Animal::getRandomName(),   Animal::getRandomColor(), Animal::getRandomGender() ); break;
   }

   return newAnimal;

}


} // namespace animalfarm
