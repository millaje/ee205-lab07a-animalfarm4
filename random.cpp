#include<iostream>
#include<random>
#include<chrono>

using namespace std;

int main(){
	unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std:: default_random_engine rd(seed);
	cout << "random: " << rd() << endl;
	cout << "minmum : " << rd.min() << endl;
	cout << "maximum : " << rd.max() << endl;
	return 0;
}

