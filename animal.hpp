///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 Mar 2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
using namespace std;
#include <string>
#include <cstdlib>
#include "node.hpp"

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };  /// @todo Add more colors

class Animal : public Node{
public: 
      Animal();
      ~Animal();

public:
      enum Gender gender;
	   string      species;
public:
	   virtual const string speak() = 0;
	   void printInfo();
public:
	   string colorName  (enum Color color);
	   string genderName (enum Gender gender);
   
   static const Gender  getRandomGender();
   static const Color   getRandomColor();
   static const bool    getRandomBool();
   static const float   getRandomWeight(const float from, const float to);
   static const string  getRandomName();
};

class AnimalFactory{
   public:
      static Animal* getRandomAnimal();
};


} // namespace animalfarm
