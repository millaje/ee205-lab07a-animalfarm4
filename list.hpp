///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 Mar 2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <iostream>
#include <cstdlib>
#include "node.hpp"


using namespace std;

namespace animalfarm{

   class SingleLinkedList{
      protected:
         Node* head = nullptr;
         unsigned int numOfNodes =0;

      public:
         const bool empty() const;
         void push_front(Node* newNode);
         Node* pop_front();
         Node* get_first() const;
         Node* get_next(const Node* currentNode) const;
         unsigned int size() const;
   };
   

}

