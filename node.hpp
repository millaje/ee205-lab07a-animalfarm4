///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.pp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 Mar 2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>
//#include <cstdlib>



using namespace std;
namespace animalfarm{
   class Node{
      protected:
         Node* next = nullptr;
         friend class SingleLinkedList;
   };
}



